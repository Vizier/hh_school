#!/usr/bin/python3
"""
File: problem1.py
Author: Pavel Pavlov
Description: Solution to problem 1

PROBLEM SPECIFICATION
---------------------

Given N points on the plane, find minimal distance
between two points. For simplicity, it might be assumed
that all points have integer coordinates.

RUNNING SCRIPT
--------------

This script is compatible with Python 3 and PyPy.

If ran without arguments, it reads coordinates of points
from standard input. Coordinates should be two numbers
separated by whitespace.

    python problem1.py 
    2 2
    2 3
    5 8

    1.0

    cat points.txt | python problem1.py
    5.0

Alternatively, points could be read from file

    python problem1.py -f points.txt

    python problem1.py --file="points.txt"

Structure of the file should be the same - two numbers separated
by whitespace on each line, representing x and y coordinates.

"""

import math
import sys
import argparse


class DataFormatError(Exception):
    pass


class Point(object):

    """Point on the plane, defined by it's coordinates."""

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def distance_to(self, other):
        """Return distance to other Point."""
        return math.sqrt(self.squared_distance_to(other))

    def squared_distance_to(self, other):
        """Return square of distance to other Point."""
        return (self.x - other.x) ** 2 + (self.y - other.y) ** 2

    def __repr__(self):
        return "Point({}, {})".format(self.x, self.y)


def find_min_distance(points):
    """
    Given a list of Points on plane,
    return minimal distance between two points.
    """
    if len(points) < 2:
        return float('inf')

    sorted_by_x = sorted(points, key=lambda p: p.x)

    # auxiliary array to store intermediate data
    aux = [None] * len(points)

    return _min_distance(sorted_by_x, list(sorted_by_x), aux, 0, len(sorted_by_x) - 1)


def _min_distance(points, points_by_y, aux, low, high):
    """
    Return minimal distance between two points in sublist 
    of Points starting with low and ending with high.
    Sort points_by_y by y-coordinate as side-effect.

    Assumes that:

        points is a list of Points sorted by x coordinate;

        points_by_y is a list of Points such that points
        and points_by_y are the same set of Points
        in range [low, high];

        aux is an auxiliary list that has the same size as 
        points array and it's content can be overwritten.

    """
    if high <= low:
        return float('inf')

    # Split points in two halves by vertical line and compute
    # minimal distance in each subpart.
    mid = low + (high - low) // 2
    distance = min(_min_distance(points, points_by_y, aux, low, mid),
                   _min_distance(points, points_by_y, aux, mid + 1, high))

    # If we keep points_by_y sorted by y-coordinate in
    # each subpart, we can merge them back together
    # in linear time and get points_by_y sorted
    # in range from low to high inclusive.  
    _sort_by_merging(points_by_y, aux, low, mid, high)

    # Now we need to check points near the vertical line we
    # used for splitting. Specifically, the points that are closer
    # to vertical line than computed distance (shown by *)
    #
    #                        |            
    #                        |            
    #            *           | *          
    #                        |            
    #                        |            
    #                        |                       x
    #                        |            
    #                        |            
    #                        |            
    #                        |            
    #                        |            
    #                        |            
    #                        |            
    #                    *   |                        
    #                        |  *       
    #  x                     |                    x
    #       <-- distance --> | <-- distance -->
    k = 0
    mid_point = points[mid]
    for i in range(low, high + 1):
        if abs(points_by_y[i].x - mid_point.x) < distance:
            aux[k] = points_by_y[i]
            k += 1

    # Finally, each of those points should be compared to 
    # points with y-coordinate closer then distance.
    # It can be shown that there could be at most 7 such points.
    #
    #                             coincident points
    #                             (one in left, one in right)
    #                           /
    #                          / 
    #                         /
    #           -- *---------*---------*
    #           ^  |         |         |
    # distance  |  |  left   |  right  |
    #           |  |         |         |
    #           v  |         |         |
    #           -- *---------*---------*
    #              |<------->|\------->|
    #               distance   \ distance
    #                           \
    #                             coincident points
    #                             (one in left, one in right)
    for i in range(k):
        for j in range(i + 1, k):
            if aux[j].y - aux[i].y >= distance:
                break
            distance = min(distance, aux[i].distance_to(aux[j]))

    return distance


def _sort_by_merging(points, aux, low, mid, high):
    """
    Given low, mid and high indices of sublists' borders,
    sort points sublist from low to high inclusive
    by y-coordinate by merging it's two sorted sublists
    from low to mid inclusive and
    from mid + 1 to high inclusive. 

    Assumes that:
        points are already sorted by y-coordinate in ranges
            from low to mid inclusive and
            from mid + 1 to high inclusive 

        aux is an auxiliary list that has the same size as 
        points array and it's content can be overwritten.

    """
    aux[low : high + 1] = points[low : high + 1]

    left = low
    right = mid + 1

    for next_index in range(low, high + 1):
        if left > mid:
            points[next_index : high + 1] = aux[right : high + 1]
            return
        elif right > high:
            points[next_index : high + 1] = aux[left : mid + 1]
            return
        elif aux[left].y < aux[right].y:
            points[next_index] = aux[left]
            left += 1
        else:
            points[next_index] = aux[right]
            right += 1


def _read_points_from_file(filename):
    """
    Read coordinates from file and return a list of Points.

    Assumes that each line of file contains X and Y coordinates
    of a point separated by whitespace.

    Raise IOError if file cannot be found or read.
    Raise DataFormatError if some line doesn't contain two numbers
    separated by whitespace.
    """
    with open(filename, mode='r') as f:
        return _read_points(f)


def _read_points(input_stream):
    """
    Read coordinates from input stream and return a list of Points.

    Assumes that each line of stream contains X and Y coordinates
    of a point separated by whitespace.

    Raise DataFormatError if some line doesn't contain two numbers
    separated by whitespace.
    """
    points = []
    line_number = 0

    try:
        while True:
            line_number += 1
            line = input_stream.readline().strip()
            coordinates = [float(num) for num in line.split()]
            if not coordinates:
                break
            x, y = coordinates
            points.append(Point(x, y))
    except ValueError:
        raise DataFormatError(
                "Wrong data format in line {}: expected 2 numbers, got '{}'.".format(
                line_number, line))

    return points


def _main(args):
    try:
        if args.file:
            points = _read_points_from_file(args.file)
        else:
            points = _read_points(sys.stdin)
    except (IOError, DataFormatError) as e:
        sys.exit("{}".format(sys.exc_info()[1]))

    print(find_min_distance(points))


def _parse_args():
    description = ("Print minimal distance between two points on plane. "
                   "Points are read from standard input as X and Y coordinates "
                   "on each line separated by whitespace")
    parser = argparse.ArgumentParser(
            prog="problem2",
            description=description)
    parser.add_argument("-f", "--file", help="read points from file", default=None)
    return parser.parse_args()


if __name__ == '__main__':
    _main(_parse_args())
