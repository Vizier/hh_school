"""
File: problem2.py
Author: Pavel Pavlov
Description: solution to problem 2

PROBLEM SPECIFICATION
---------------------

For two given natural numbers n and k find the number of ways to 
represent n as a sum of k summands if sums that are differ only in
order of their summands are considered the same representation.

n <= 150, k <= 150

IMPLEMENTATION NOTES
--------------------

This problem can be solved using following recurrence:

    P(n, k) = P(n - 1, k - 1) + P(n - k, k)

with base cases:

    P(i, i) = 1,
    P(i, 1) = 1,
    P(i, j) = 0 for j > i, 
    
    where i, j are natural numbers.

Straightforward recursive solution can be implemented using the above
recurrence. With memoization technique applied, it can show decent 
performance for the input limits stated in the specification.

However, another approach is taken - solution is built bottom up
iteratively.
With iterative approach input data limits are no longer bounded by
recursion depth limit and could be expanded significantly.


RUNNING SCRIPT
--------------

This script is Python 3 compatible.

It can be run with two positional arguments NUMBER and NUM_PARTS:

    python problem2.py 6 3
    3

If any or both positional arguments are not specified,
they are read from standard input:

    python problem2.py
    6 3
    3

"""

import sys
import argparse


def num_partitions(number, num_summands):
    """
    Given number and num_summands, return the number of ways
    to partition number into a sum of natural numbers with
    num_summands summands.

    number and num_summands should be integers (or could be
    casted to integer) that are also natural numbers

    Raise ValueError if number or num_summands are not integers
    that are natural numbers.

    """
    number = int(number)
    num_summands = int(num_summands)

    if number < 1 or num_summands < 1:
        err_msg = "Natural numbers expected, got ({}, {})".format(
                number, num_summands)
        raise ValueError(err_msg)

    if number < num_summands:
        return 0
    if number == num_summands or num_summands == 1:
        return 1

    aux = [1] * number

    for k in range(2, num_summands + 1):
        for n in range(k + 1, number - k + 2):
            aux[n] += aux[n - k]

    return aux[number - num_summands + 1]


def _main(args):
    if args.NUMBER and args.NUM_SUMMANDS:
        number, num_summands = args.NUMBER, args.NUM_SUMMANDS
    else:
        try:
            line = sys.stdin.readline().strip()
            number, num_summands = line.split()
        except ValueError as e:
            sys.exit(("Wrong number of arguments: "
                      "expected two natural numbers, got '{}'".format(line)))

    try:
        print(num_partitions(number, num_summands))
    except ValueError as e:
        sys.exit("{}".format(sys.exc_info()[1]))


def _parse_args():
    description = ("Print number of ways to partition NUMBER into sum "
                   "of NUM_PARTS summands if sums that differ only in order "
                   "of their summands are considered the same partition. "
                   "If positional arguments NUMBER and NUM_PARTS are not "
                   "specified, read them from standard input.")
    
    parser = argparse.ArgumentParser(
            prog="problem2",
            description=description
    )
    parser.add_argument('NUMBER',
                        help="number to partition",
                        type=int,
                        nargs="?")
    parser.add_argument('NUM_SUMMANDS',
                        help="number of parts (summands)",
                        type=int,
                        nargs="?")

    return parser.parse_args()


if __name__ == '__main__':
    _main(_parse_args())
