import unittest
from problem2 import num_partitions


class TestProblem2(unittest.TestCase):

    def test_raises_exception_when_given_non_natural_numbers(self):
        with self.assertRaises(ValueError):
            num_partitions(-3, 2)
        with self.assertRaises(ValueError):
            num_partitions(3, -8)
        with self.assertRaises(ValueError):
            num_partitions(0, 8)

    def test_returns_zero_when_impossible_to_partition(self):
        self.assertEqual(0, num_partitions(5, 6))

    def test_returns_one_when_number_equals_to_number_of_parts(self):
        self.assertEqual(1, num_partitions(3, 3))
        
    def test_returns_one_when_only_one_part_left(self):
        self.assertEqual(1, num_partitions(3, 1))

    def test_computes_number_of_partitions_correctly(self):
        self.assertEqual(3, num_partitions(6, 3))
        self.assertEqual(2, num_partitions(5, 2))
        self.assertEqual(3, num_partitions(7, 2))
