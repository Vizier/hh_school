import unittest
import random
import math
from itertools import combinations
from problem1 import Point, find_min_distance, _read_points_from_file, DataFormatError, _sort_by_merging


DELTA = 0.00000001


class TestPoint(unittest.TestCase):

    def test_distance_to_self_should_be_zero(self):
        point = Point(3, 2)
        self.assertAlmostEqual(0, point.squared_distance_to(point), delta=DELTA)

    def test_computes_distance_correctly(self):
        self.assertAlmostEqual(2,
                               Point(2, 2).distance_to(Point(2, 4)),
                               delta=DELTA)


class TestProblem1(unittest.TestCase):

    def find_min_distance_brute_force(self, points):
        """
        Given a list of Points on plane,
        return minimal distance between two points.
        """
        return math.sqrt(min(p1.squared_distance_to(p2)
            for p1, p2 in combinations(points, 2)))

    def test_returns_infinity_if_no_points(self):
        self.assertEqual(float('inf'), find_min_distance([]))

    def test_returns_infinity_if_only_one_point(self):
        self.assertEqual(float('inf'), find_min_distance([Point(3, 4)]))

    def test_returns_zero_if_two_equal_points(self):
        points = [Point(1, 2), Point(1, 2)]
        self.assertAlmostEqual(0, find_min_distance(points), delta=DELTA)

    def test_find_min_distance(self):
        points = [Point(0, 2),
                  Point(6, 67),
                  Point(43, 71),
                  Point(39, 107),
                  Point(189, 140)]
        expected = 36.2215
        self.assertAlmostEqual(expected, find_min_distance(points), places=4)

    def test_divide_and_conquer_results_should_be_same_as_brute_force(self):
        for _ in range(1000):
            points = [Point(random.randint(-1000, 1000),
                            random.randint(-1000, 1000)) for _ in range(20)]
            self.assertAlmostEqual(
                    self.find_min_distance_brute_force(points),
                    find_min_distance(points),
                    delta=DELTA)

    def test_read_points_from_file_happy_path(self):
        expected = 2.2361
        points = _read_points_from_file("tests/data/points.txt")
        self.assertAlmostEqual(expected, find_min_distance(points), places=4)
        
    def test_read_points_from_file_sad_path(self):
        with self.assertRaises(DataFormatError):
            _read_points_from_file("tests/data/incorrect.txt")
